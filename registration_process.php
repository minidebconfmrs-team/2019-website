<?php
exit;
//    $organizer_mail = "orga-minidebconf@france.debian.net";
//
//    $email = $_REQUEST['email'];
//    $name = $_REQUEST['name'];
//    $arrival = $_REQUEST['arrival'];
//    $departure = $_REQUEST['departure'];
//    $origin = $_REQUEST['origin'];
//    $gpg = $_REQUEST['gpg'];
//    $tshirt = $_REQUEST['tshirt'];
//    $food = $_REQUEST['food'];
//    $photo = $_REQUEST['photo'];
//    $socialevent = $_REQUEST['socialevent'];
//    $daytrip = $_REQUEST['daytrip'];
//    $comments = $_REQUEST['comments'];
//
//  // Mail to the organizers
//    $to = $organizer_mail;
//
//    $headers  = "From: Website mini-DebConf MRS <{$organizer_mail}>\r\n";
//    $headers .= "MIME-Version: 1.0\r\n";
//    $headers .= "Content-Type: text/plain; charset=UTF-8\r\n";
//
//    $subject = "{$email} has just registered";
//
//    $body  = "name: {$name}\r\n";
//    $body .= "email: {$email}\r\n";
//    $body .= "origin: {$origin}\r\n";
//    $body .= "arrival: {$arrival}\r\n";
//    $body .= "departure: {$departure}\r\n";
//    $body .= "gpg: {$gpg}\r\n";
//    $body .= "tshirt: {$tshirt}\r\n";
//    $body .= "photo: {$photo}\r\n";
//    $body .= "beer-event: {$socialevent}\r\n";
//    $body .= "day-trip: {$daytrip}\r\n";
//    $body .= "food: {$food}\r\n";
//    $body .= "comments: {$comments}\r\n";
//
//    $send = mail($to, $subject, $body, $headers);
//
//  // Mail to the registrant
//    $to = $email;
//
//    $headers  = "From: Mini-DebConf Marseille <{$organizer_mail}>\r\n";
//    $headers .= "MIME-Version: 1.0\r\n";
//    $headers .= "Content-Type: text/plain; charset=UTF-8\r\n";
//
//    $subject = "Thanks for your registration";
//
//    $body  = "Hi {$name}\r\n";
//    $body .= "You've just filled the registration form for the Mini-DebConf Marseille.\r\n";
//    $body .= "\r\n";
//    $body .= "For the record, here is a copy of what you've filled the form with.\r\n";
//    $body .= "Name: {$name}\r\n";
//    $body .= "Email: {$email}\r\n";
//    $body .= "Origin: {$origin}\r\n";
//    $body .= "Arrival: {$arrival}\r\n";
//    $body .= "Departure: {$departure}\r\n";
//    $body .= "GPG: {$gpg}\r\n";
//    $body .= "T-shirt preference: {$tshirt}\r\n";
//    $body .= "Photo policy: {$photo}\r\n";
//    $body .= "Beer-event: {$socialevent}\r\n";
//    $body .= "Day-trip: {$daytrip}\r\n";
//    $body .= "Food constraints: {$food}\r\n";
//    $body .= "Additional comments: {$comments}\r\n";
//    $body .= "\r\n";
//    $body .= "We'll get back to you with additional information before the event.\r\n";
//    $body .= "You also can write to <{$organizer_mail}> if you need.\r\n";
//    $body .= "\r\n";
//    $body .= "Keep an eye on the wiki, we add information regularly :\r\n";
//    $body .= "https://wiki.debian.org/DebianEvents/fr/2019/Marseille\r\n";
//    $body .= "\r\n";
//    $body .= "Here are a few key information :\r\n";
//    $body .= "* Speakers can ask for Debian sponsorship, covering travel expanses\r\n";
//    $body .= "* European elections happen during the weekend so don't forget to plan :\r\n";
//    $body .= "  https://wiki.debian.org/DebianEvents/fr/2019/Marseille#FAQ\r\n";
//    $body .= "* We need help to put up a good event, say Hi if you want to volunteer.\r\n";
//    $body .= "* You can still submit a talk proposal on the mailing list, don't be shy :\r\n";
//    $body .= "  https://france.debian.net/mailman/listinfo/minidebconf\r\n";
//    $body .= "\r\n";
//    $body .= "We hope to see you in Marseille on May 25th/26th.\r\n";
//    $body .= "\r\n";
//    $body .= "-- \r\n";
//    $body .= "The Mini-DebConf organization team\r\n";
//    $body .= "https://wiki.debian.org/DebianEvents/fr/2019/Marseille\r\n";
//    $body .= "https://minidebconf-mrs.debian.net/\r\n";
//    $body .= "https://twitter.com/minidebconf_mrs – https://mamot.fr/@minidebconf_mrs\r\n";
//    $body .= "#debconf-marseille on irc.debian.org\r\n";
//
//
//    $send = mail($to, $subject, $body, $headers, "-f {$organizer_mail}");
//
//    // Save data on disk
//
//    $file_name = "/home/minidebconfmrs/registrations/" . time() . ".txt";
//    $fp = fopen($file_name,"a+");
//    fwrite($fp,var_export($_REQUEST,true));
//    fclose($fp);
//
//    echo "Thank you for your registration";
?>
